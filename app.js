// Source Connection
const express = require("express");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./router/userRoutes");
const productRoutes = require("./router/productRoutes");
const orderRoutes = require("./router/orderRoutes");

dotenv.config();

const app = express();
// const port = 7777;

// MongoDB Connection
mongoose.connect(
  `mongodb+srv://cjmacaraeg900:${process.env.MONGODB_PASSWORD}@cluster0.it74nuq.mongodb.net/capstone3-ecommerce-system-API?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("open", () => console.log("Connected to MongoDB!"));
// MongoDB Connection END

// To avoid CORS error when trying to send request to our server
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// Routes END

// app.listen(port, () => console.log(`API is now running on localhost:${port}`));
app.listen(process.env.PORT || 7777, () => {
    console.log(`Connected to localhost:${process.env.PORT || 7777}!`)
})