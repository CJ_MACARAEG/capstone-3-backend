const express = require("express");
const router = express.Router();
const OrderController = require("../controllers/OrderController");
const UserController = require('../controllers/UserController')
const auth = require("../auth");

function UserOnleMiddleware(request, response, next) {
  const token = request.headers.authorization;
  const user = auth.decode(token);

  if (user.isAdmin == false) {
    next();
  } else {
    response.status(401).send("You are not an user");
  }
}

// ================= OPTION 1 ====================
// Create Order
// router.post("/createOrder", auth.verify, (request, response) => {
//   // const token = request.headers.authorization;
//   // const user = auth.decode(token);
//   const user = request.body;
//   const body = request.body;
//   const products = request.body;
//   // console.log(user);
//   console.log(body);

//   OrderController.addOrder(user.id, body.id, body.amount).then((result) => {
//     response.send(result);
//   });
// });

// ===================== Option 2 ======================

// Create Order
// router.post('/createOrder', auth.verify, UserOnleMiddleware, (request, response) => {
//   const data = {
//     // isAdmin: auth.decode(request.headers.authorization).isAdmin,
//     userId: request.body.userId,
//     products: request.body.products,
//     productId: request.body.productId,
//     quantity: request.body.quantity,
//     price: request.body.price
//   }

//   OrderController.createOrder(data).then((result) => {
//     response.send(result)
//   })
// })


// ===================== Option 3 ======================

router.post('/create-order', auth.verify, (request, response) => {
  
  const data = {
          order: request.body,
          isAdmin: auth.decode(request.headers.authorization).isAdmin
      }
  const userId = auth.decode(request.headers.authorization).id
  
  
  OrderController.createOrder(data, userId, request.body).then((result) => {
    response.send(result)
  })
})

// Retrieve all orders (Admin only)
router.get('/all-orders', (request, response) => {
  OrderController.showAllOrders().then((result) => {
    response.send(result)
  })
})

// router.get('/:userId/user-order', auth.verify, (request, response) => {

//   OrderController.userOrders(request.params.userId).then((result) => {
//     response.send(result)
//   })
// })

// // Retrieve authenticated user’s orders END


// router.get('/user-order', auth.verify, (request, response) => {

//   const userId = auth.decode(request.headers.authorization).id

//   OrderController.userOrders(userId).then(result => {
//     response.send(result)
//   })

// })




// // Get Order List
// router.get("/getOrderList", auth.verify, (request, response) => {
//   OrderController.getOrders(user.id).then((result) => {
//     response.send(result);
//   });
// });

module.exports = router;
