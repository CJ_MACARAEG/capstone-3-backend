const User = require('../models/User')
// const Product = require('../models/Product')
// const Order = require('../models/Order')
const bcrypt = require('bcrypt')
// const auth = require('../auth')

// User Registration Controller
module.exports.register = (data) => {
	let encypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		email: data.email,
		password: encypted_password,
        isAdmin: true
	})

	return new_user.save().then((created_user, error) => {
		if(error){
			return false
		}

		return {
			message: 'Admin successfully registered!'
		}
	})
}

// User login Controller
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User doesn't exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
			return {
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}
	})
}

// Retrieve User/Admin Details Controller
module.exports.getUserDetails = (user_id) => {
    return User.find(user_id, {password: 0}).then((result) => {
        return result
    })
}

module.exports.deleteTask = (user_id) => {
	return User.findByIdAndDelete(user_id, {isAdmin: false}).then((removedTask, error) => {
		if(error){
			console.log(error)
				return error
			}

		return {
			message: 'The User has been deleted'
		}
	})
}

